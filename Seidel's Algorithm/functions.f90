module functions
implicit none
contains

recursive function Seidel(A, num) result(D)

integer :: num
double precision, dimension(num, num) :: A, Z, B, T, X
integer, dimension(num, num) :: D
integer :: i, j
double precision, parameter :: alpha = 1.0, beta = 0.0
logical :: tf
double precision, dimension(num) :: degree

degree = 0
do i = 1, num
        do j = 1, num
                degree(i) = degree(i) + A(i,j)
        end do
end do

tf = .TRUE.

call dgemm('n', 'n', num, num, num, alpha, A, num, A, num, beta, Z, num)

do i = 1, num
        do j = 1, num
                if ((i /= j) .AND. (A(i, j) == 1 .OR. Z(i, j) > 0)) then
                        B(i, j) = 1
                        else
                        B(i, j) = 0
                end if
        end do
end do

do i = 1, num
        do j = 1, num
                if ((i /= j) .AND. (B(i, j) /= 1)) then
                        tf = .FALSE.
                end if
        end do
end do

if (tf == .TRUE.) then
        D = int(2*B - A)
else
        T = Seidel(B, num)
        call dgemm('n','n', num, num, num, alpha, T, num, A, num, beta ,X, num)
        do i = 1, num
                do j = 1, num
                        if (X(i, j) >= T(i, j)*degree(j)) then
                                 D(i, j) = int(2*T(i, j))
                        else
                                 D(i, j) = int(2*T(i, j) - 1)
                        end if
                end do
        end do
end if

end function Seidel

function mindistance(D, num, i, j)
integer :: num, i, j, mindistance
integer :: D(num, num)

mindistance = D(i,j)
end function mindistance

function avgnpdistance(D, num)
integer :: num, i, j
integer :: D(num, num)
real :: avgnpdistance

do i = 1, num
        do j = 1, num
                avgnpdistance = avgnpdistance + D(i,j)
        end do
end do

avgnpdistance = avgnpdistance / (num**2 - num)
end function avgnpdistance

function ecc(D, num, j)
integer :: j, num, ecc
integer :: D(num, num)

ecc = maxval(D(:, j))
end function ecc     

function radius(D, num)
integer :: i, num, radius
integer :: D(num, num) 
integer :: total(num)

do i = 1, num
        total(i) = maxval(D(:,i))
end do
radius = minval(total)
end function radius

function diameter(D, num)
integer :: i, num, diameter
integer :: D(num, num)
integer :: total(num)

do i = 1, num
        total(i) = maxval(D(:,i))
end do
diameter = maxval(total)
end function diameter

end module functions
